package lab3;

import javafx.geometry.Point2D;
import javafx.util.Pair;
import lab3.cMath.Func;
import lab3.cMath.FuncOnPoint;
import lab3.cMath.chart;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by idib on 27.03.17.
 */
public class main {

    public static int dif(int a, int b) {
        int n = 32;
        int d = 0;
        for (int i = 0; i < n; i++)
            if ((a & (1 << i)) != (b & (1 << i)))
                d++;
        return d;
    }

    public static int dif(Pair<Integer, Integer> a, Pair<Integer, Integer> b) {
        return dif(a.getKey(), b.getKey()) + dif(a.getValue(), b.getValue());
    }

    public static void main(String[] args) {
        String originalString = "String0";
        String originalString2 = "String1";
        String Key = "37373lfmslidnfgsanboigfa psjf092";
        String Key2 = "37373lfmslidnfgsanboigfa psjf092";

        List<List<Pair<Integer, Integer>>> mem = new ArrayList<>();
        System.out.println(GOST.Gost28147_89EncodeBasic(originalString, Key, mem).length);
        List<List<Pair<Integer, Integer>>> mem2 = new ArrayList<>();
        System.out.println(Arrays.toString(GOST.Gost28147_89EncodeBasic(originalString2, Key2, mem2)));
        plot(mem,mem2);
    }

    public static void plot(List<List<Pair<Integer, Integer>>> a, List<List<Pair<Integer, Integer>>> b) {
        List<Func> fl = new ArrayList<>();
        FuncOnPoint f = new FuncOnPoint("название");
        for (int i = 0; i < a.get(0).size(); i++) {
            int maxd = 0;
            for (int j = 0; j < a.size(); j++) {
                maxd += dif(a.get(j).get(i), b.get(j).get(i));
            }
            f.add(new Point2D(i, maxd));
        }
        fl.add(f);
        chart.srun("", fl, -0.1, a.get(0).size());
    }
}



