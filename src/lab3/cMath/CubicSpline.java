package lab3.cMath;

import java.util.List;

/**
 * Created by idib on 22.03.17.
 */
public class CubicSpline extends Func {
    SplineTuple[] splines;

    public CubicSpline(List<Double> x, List<Double> y) {
        super("CubicSpline");
        init(x,y);
    }

    public CubicSpline(String name,List<Double> x, List<Double> y) {
        super(name);
        init(x,y);
    }

    private void init(List<Double> x, List<Double> y){
        int n = x.size();
        splines = new SplineTuple[n];
        for (int i = 0; i < n; ++i) {
            splines[i] = new SplineTuple();
            splines[i].x = x.get(i);
            splines[i].a = y.get(i);
        }
        splines[0].c = splines[n - 1].c = (double) 0.0;

        double[] alpha = new double[n - 1];
        double[] beta = new double[n - 1];
        alpha[0] = beta[0] = (double) 0.0;
        for (int i = 1; i < n - 1; ++i) {
            double hi = x.get(i) - x.get(i - 1);
            double hi1 = x.get(i + 1) - x.get(i);
            double A = hi;
            double C = 2 * (hi + hi1);
            double B = hi1;
            double F = 6 * ((y.get(i + 1) - y.get(i)) / hi1 - (y.get(i) - y.get(i - 1)) / hi);
            double z = (A * alpha[i - 1] + C);
            alpha[i] = -B / z;
            beta[i] = (F - A * beta[i - 1]) / z;
        }

        for (int i = n - 2; i > 0; --i) {
            splines[i].c = alpha[i] * splines[i + 1].c + beta[i];
        }


        for (int i = n - 1; i > 0; --i) {
            double hi = x.get(i) - x.get(i - 1);
            splines[i].d = (splines[i].c - splines[i - 1].c) / hi;
            splines[i].b = hi * (2 * splines[i].c + splines[i - 1].c) / 6 + (y.get(i) - y.get(i - 1)) / hi;
        }
    }

    public double calc(double x) {

        int n = splines.length;
        SplineTuple s;

        if (x <= splines[0].x) {
            s = splines[0];
        } else if (x >= splines[n - 1].x) {
            s = splines[n - 1];
        } else {
            int i = 0;
            int j = n - 1;
            while (i + 1 < j) {
                int k = i + (j - i) / 2;
                if (x <= splines[k].x) {
                    j = k;
                } else {
                    i = k;
                }
            }
            s = splines[j];
        }

        double dx = x - s.x;
        return s.a + (s.b + (s.c / 2 + s.d * dx / 6) * dx) * dx;
    }

    private class SplineTuple {
        public double a, b, c, d, x;
    }
}
