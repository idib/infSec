package lab3.cMath;

import javafx.geometry.Point2D;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by idib on 16.04.17.
 */
public abstract class Func {
    public String Name;

    public Func(String name) {
        Name = name;
    }

    public abstract double calc(double x);

    public List<Point2D> getPoints(double start, double finish) {
        int n = 1000;
        double step = (finish - start) / n;
        List<Point2D> res = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            double x = start + i*step;
            res.add(new Point2D(x,calc(x)));
        }
        return res;
    }
}
