package lab3.cMath;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Created by idib on 03.02.17.
 */
public class Matrix implements Cloneable {
    public int countIterations;
    int n, m, countSwap; // count rows, coloums
    double initValue;
    double mat[][];
    double eps = (double) 1E-7;
    private double lambdaMax = Double.MIN_VALUE;
    private double lambdaMin = Double.MIN_VALUE;
    private double lambda;

    public Matrix(int n, int m) {
        init(n, m, 0);
    }

    public Matrix(int n, int m, double initValue) {
        init(n, m, initValue);
    }

    public Matrix(String pathFile) throws FileNotFoundException {
        Scanner in = new Scanner(new File(pathFile));
        init(in.nextInt(), in.nextInt(), 0);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                mat[i][j] = in.nextFloat();
            }
        }
        in.close();
    }

    private static Matrix MatrixUkl(int k, int l, double a1, double b1, int n) {
        Matrix res = getE(n);
        res.mat[k][k] = res.mat[l][l] = a1;
        res.mat[k][l] = -b1;
        res.mat[l][k] = b1;
        return res;
    }

    public static Matrix add(Matrix l, Matrix r) {
        if (l.n != r.n || l.m != r.m)
            throw new NumberFormatException("left matrix.n != right matrix.n or left matrix.m != right matrix.m");
        Matrix res = new Matrix(l.n, l.m);
        for (int i = 0; i < l.n; i++)
            for (int j = 0; j < l.m; j++)
                res.mat[i][j] = l.mat[i][j] + r.mat[i][j];
        return res;
    }

    public static Matrix getE(int n) {
        Matrix res = new Matrix(n, n, 0);
        for (int i = 0; i < n; i++) {
            res.mat[i][i] = 1;
        }
        return res;
    }

    public static Matrix mul(double l, Matrix r) {
        return mul(r, l);
    }

    public static Matrix mul(Matrix l, Matrix r) {
        if (l.m != r.n)
            throw new NumberFormatException("left matrix.m != right matrix.n");
        Matrix res = new Matrix(l.n, r.m);
        for (int i = 0; i < l.n; i++)
            for (int j = 0; j < r.m; j++)
                for (int k = 0; k < l.m; k++)
                    res.mat[i][j] += l.mat[i][k] * r.mat[k][j];
        return res;
    }

    public static Matrix mul(Matrix l, double r) {
        Matrix res = new Matrix(l.n, l.m);
        for (int i = 0; i < l.n; i++)
            for (int j = 0; j < l.m; j++)
                res.mat[i][j] = l.mat[i][j] * r;
        return res;
    }

    public static double scalar(Matrix x, Matrix y) throws Exception {
        double sum = 0;
        if (x.n != y.n || x.m != 1 || y.m != 1)
            throw new Exception("scalar definition for vector");
        int n = x.n;
        for (int i = 0; i < n; i++) {
            sum += x.mat[i][0] * y.mat[i][0];
        }
        return sum;
    }

    public static Matrix sub(Matrix l, Matrix r) {
        if (l.n != r.n || l.m != r.m)
            throw new NumberFormatException("left matrix.n != right matrix.n or left matrix.m != right matrix.m");
        Matrix res = new Matrix(l.n, l.m);
        for (int i = 0; i < l.n; i++)
            for (int j = 0; j < l.m; j++)
                res.mat[i][j] = l.mat[i][j] - r.mat[i][j];
        return res;
    }

    private boolean Range(int i, int j) {
        if (i >= 0 && i < n && j >= 0 && j < m)
            return true;
        return false;
    }

    private boolean RangeM(int j) {
        if (j >= 0 && j < m)
            return true;
        return false;
    }

    private boolean RangeN(int i) {
        if (i >= 0 && i < n)
            return true;
        return false;
    }

    private void cloneTo(Matrix t) {
        if (t.m != m || t.n == n) {
            t.init(n, m, 0);
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                t.mat[i][j] = mat[i][j];
            }
        }
    }

    private double converge(double[] xk, double[] xkp) {
        double norm = 0;
        for (int i = 0; i < n; i++) {
            norm += (xk[i] - xkp[i]) * (xk[i] - xkp[i]);
        }
        return (double) Math.sqrt(norm);
    }

    private void delComponent(Matrix e, Matrix g) throws Exception {
        this.mat = sub(mul(scalar(g, this) / scalar(e, g), e), this).mat;
    }

    private double[][] getCopyMat() {
        double[][] newMat = new double[mat.length][];
        for (int i = 0; i < mat.length; i++)
            newMat[i] = mat[i].clone();
        return newMat;
    }

    private void init(int n, int m, double initValue) {
        this.n = n;
        this.m = m;
        this.initValue = initValue;
        this.countSwap = 0;
        mat = new double[n][m];
        for (int i = 0; i < n; i++)
            Arrays.fill(mat[i], initValue);
    }

    public Matrix FindMinLambda(Matrix x, double e) throws Exception {
        double c = lambdaMax;
        Matrix b;
        if (c > 0)
            b = sub(this, mul(c, getE(n)));
        else
            b = add(this, mul(c, getE(n)));
        b = b.LambdaWithVector(x, e);

        if (c > 0)
            b.lambdaMax += c;
        else
            b.lambdaMax -= c;
        lambdaMin = b.lambdaMax;
        return b;
    }

    public Matrix Gauss() {
        Matrix x = new Matrix(n, 1);
        for (int i = n - 1; i >= 0; i--) {
            double sum = 0;
            for (int j = i + 1; j < n; j++)
                sum += x.mat[j][0] * mat[i][j];
            x.mat[i][0] = (mat[i][m - 1] - sum) / mat[i][i];
        }
        return x;
    }

    public Matrix Jacobi(Matrix B, double epss) throws Exception {
        boolean fl = true;

        for (int i = 0; fl && i < n; i++) {
            double sum = 0;
            for (int j = 0; j < n; j++)
                if (i != j)
                    sum += Math.abs(mat[i][j]);
            if (mat[i][i] < eps || Math.abs(mat[i][i]) <= sum)
                fl = false;
        }

        if (!fl) throw new Exception("uncorrect matrix A");

        Matrix X = new Matrix(n, 1);
        Matrix Xtemp = new Matrix(n, 1);
        X.countIterations = 0;
        double diff;
        do {
            for (int i = 0; i < n; i++) {
                Xtemp.mat[i][0] = B.mat[i][0];
                for (int j = 0; j < n; j++)
                    if (i != j)
                        Xtemp.mat[i][0] -= mat[i][j] * X.mat[j][0];
                Xtemp.mat[i][0] /= mat[i][i];
            }
            diff = MaxDifference(X, Xtemp);
            Xtemp.cloneTo(X);
            X.countIterations++;
        } while (diff > epss);
        return X;
    }

    public List<Matrix> Lambda(double eps) {
        List<Matrix> res = new ArrayList<>();

        Matrix A = clone();
        Matrix C;
        Matrix B;
        Matrix D;
        Matrix DT;
        Matrix E = getE(n);

        int l = 1, k = 0;
        double alpha;
        double beta;
        double temp;

        countIterations = 0;
        do {
            double max = Double.MIN_VALUE;
            for (int i = 0; i < n; i++) {
                for (int j = i + 1; j < m; j++) {
                    if ((i != j) && (Math.abs(A.mat[i][j]) > Math.abs(max))) {
                        max = A.mat[i][j];
                        k = i;
                        l = j;
                    }
                }
            }

            if (Math.abs(A.mat[k][k] - A.mat[l][l]) < eps)
                alpha = beta = (double) Math.sqrt(0.5);
            else {
                double nq = 2 * A.mat[k][l] / (A.mat[k][k] - A.mat[l][l]);
                temp = (double) (1 / Math.sqrt(1 + nq * nq));
                alpha = (double) Math.sqrt(0.5 * (1 + temp));
                beta = (double) (Math.signum(nq) * Math.sqrt(0.5 * (1 - temp)));
            }

            D = MatrixUkl(k, l, alpha, beta, n);
            DT = MatrixUkl(k, l, alpha, -beta, n);

            C = mul(DT, A);
            B = mul(C, D);

            if (MaxDifference(A, B) < eps) break;
            A.mat = B.mat;

            E = mul(E, D);
            countIterations++;
        } while (countIterations < 100000);

        for (int j = 0; j < m; j++) {
            Matrix tmp = new Matrix(n, 1);
            tmp.lambda = A.mat[j][j];
            for (int i = 0; i < n; i++) {
                tmp.mat[i][0] = E.mat[i][j];
            }
            res.add(tmp);
        }
        return res;
    }

    public Matrix LambdaWithVector(Matrix x, double e) throws Exception {

        double lamdaLast;
        double lamda;

        Matrix x1;
        x = x.clone();
        x1 = mul(this, x);
        lamda = scalar(x1, x) / scalar(x, x);
        x = x1;
        int count = 0;
        do {
            x1 = mul(this, x);
            lamdaLast = lamda;
            lamda = scalar(x1, x) / scalar(x, x);
            x = x1;
            x.normalize();
            count++;
        } while (Math.abs(lamda - lamdaLast) > e);
        x.countIterations = count;
        x.lambdaMax = lamda;
        lambdaMax = lamda;
        return x;
    }

    public double MaxDifference(Matrix X1, Matrix X2) {
        double Result = 0;
        double Value;
        for (int i = 0; i < X1.n; i++) {
            Value = Math.abs(X1.mat[i][0] - X2.mat[i][0]);
            if (Value > Result)
                Result = Value;
        }
        return Result;
    }

    public Matrix Seidel(Matrix B, double epss) throws Exception {
        Matrix X = new Matrix(n, 1);
        Matrix Xtemp = new Matrix(n, 1);
        X.countIterations = 0;
        double f = 0;
        do {
            for (int i = 0; i < n; i++)
                Xtemp.mat[i][0] = X.mat[i][0];

            for (int i = 0; i < n; i++) {
                double var = 0;
                for (int j = 0; j < i; j++)
                    var += (mat[i][j] * X.mat[j][0]);
                for (int j = i + 1; j < n; j++)
                    var += (mat[i][j] * Xtemp.mat[j][0]);
                X.mat[i][0] = (B.mat[i][0] - var) / mat[i][i];
            }

            f = MaxDifference(X, Xtemp);
            X.cloneTo(Xtemp);
            X.countIterations++;
        } while (f > epss);
        return X;
    }

    public Matrix abs() {
        Matrix res = new Matrix(n, m);
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                res.mat[i][j] = Math.abs(mat[i][j]);
        return res;
    }

    public void addColl(Matrix addition) {
        if (n != addition.n) throw new NumberFormatException("matrix.addCol n!=addtion.n");
        int oldm = m;
        setM(m + addition.m);
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < addition.m; ++j)
                mat[i][j + oldm] = addition.mat[i][j];
    }

    public Matrix clone() {
        Matrix t = new Matrix(n, m);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                t.mat[i][j] = mat[i][j];
            }
        }
        return t;
    }

    public String toString() {
        String str = "";
        if (m == 1) {
            for (int i = 0; i < n; i++) {
                str += Double.toString(mat[i][0]) + ' ';
            }
            if (lambdaMax != Double.MIN_VALUE)
                str += "\nLambda Max = " + Double.toString(lambdaMax);

            if (lambdaMin != Double.MIN_VALUE)
                str += "\nLambda Min = " + Double.toString(lambdaMin);

            if (lambda != Double.MIN_VALUE)
                str += "\nLambda = " + Double.toString(lambda);
        } else {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++)
                    str += Double.toString(mat[i][j]) + ' ';
                str += '\n';
            }
        }
        str += '\n';
        return str;
    }

    public double det() {
        double res = 1;
        if (n != m) throw new NumberFormatException("matrix.det n!=m");
        Matrix newMat = transformTrapezoid();
        for (int i = 0; i < n; i++) {
            res *= newMat.mat[i][i];
        }
        if (newMat.countSwap % 2 == 1)
            res *= -1;
        return res;
    }

    public Matrix findTwoLambda(Matrix x, double ep) throws Exception {
        Matrix e = LambdaWithVector(x, ep);
        Matrix g = transposed().LambdaWithVector(x, ep);


        double y = scalar(x, g) / scalar(e, g);

        Matrix y0 = sub(x, mul(y, e));

        double lamdaLast;
        double lamda;

        Matrix x1;
        x = x.clone();
        x1 = mul(this, x);
        lamda = scalar(x1, x) / scalar(x, x);
        x = x1;
        int count = 0;
        do {
            x1 = mul(this, x);
            lamdaLast = lamda;
            lamda = scalar(x1, x) / scalar(x, x);
            x = x1;
            x.normalize();
            x.delComponent(e, g);
            count++;
        } while (Math.abs(lamda - lamdaLast) > ep);
        x.countIterations = count;
        x.lambdaMin = lamda;
        lambdaMin = lamda;
        return x;
    }

    public int getM() {
        return m;
    }

    public void setM(int _m) {
        setNM(n, _m);
    }

    public double getMat(int i, int j) {
        if (Range(i, j))
            return mat[i][j];
        else
            throw new NumberFormatException("index i or j not own range");
    }

    public int getN() {
        return n;
    }

    public void setN(int _n) {
        setNM(_n, m);
    }

    public void normalize() throws Exception {
        double norm = (double) Math.sqrt(scalar(this, this));
        for (int i = 0; i < n; i++) {
            mat[i][0] = mat[i][0] / norm;
        }
    }

    public void setMat(int i, int j, double val) {
        if (Range(i, j))
            mat[i][j] = val;
        else
            throw new NumberFormatException("index i or j not own range");
    }

    public void setNM(int _n, int _m) {
        double newmat[][] = new double[_n][_m];
        for (int i = 0; i < _n; i++)
            Arrays.fill(newmat[i], initValue);
        n = Math.min(n, _n);
        m = Math.min(m, _m);
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                newmat[i][j] = mat[i][j];
        n = _n;
        m = _m;
        mat = newmat;
    }

    public Matrix sqrtMetod(Matrix B) {
        Matrix S, D;

        S = new Matrix(n, m);
        D = new Matrix(n, m);

        int[] mis = new int[n];
        for (int i = 0; i < n; i++) {
            mis[i] = i;
        }

        for (int i = 0; i < n; i++) {
            double el = mat[i][i];
            for (int j = i - 1; j >= 0; j--) {
                el -= S.mat[j][i] * S.mat[j][i] * D.mat[j][j];
            }
            D.mat[i][i] = el > 0 ? 1 : -1;
            S.mat[i][i] = (double) Math.sqrt(Math.abs(el));
            if (S.mat[i][i] < eps) {
                int t = i;
                int tem;
                if (i != 0) t++;
                swapCol(t - 1, t);
                swapRow(t - 1, t);
                B.swapRow(t - 1, t);
                tem = mis[t];
                mis[t] = mis[t - 1];
                mis[t - 1] = tem;
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < m; j++) {
                double sum = mat[i][j];
                for (int k = i - 1; k >= 0; k--) {
                    sum -= S.mat[k][i] * S.mat[k][j] * D.mat[k][k];
                }
                S.mat[i][j] = sum / (S.mat[i][i] * D.mat[i][i]);
            }
        }

        Matrix Z = new Matrix(S.n, 1);
        for (int i = 0; i < S.n; i++) {
            double sum = 0;
            for (int j = 0; j < i; ++j)
                sum += Z.mat[j][0] * S.mat[j][i];
            Z.mat[i][0] = (B.mat[i][0] - sum) / S.mat[i][i];
        }
        Matrix Y = new Matrix(S.n, 1);
        for (int i = 0; i < S.n; i++)
            Y.mat[i][0] = Z.mat[i][0] / D.mat[i][i];

        Matrix X = new Matrix(S.n, 1);
        for (int i = S.n - 1; i >= 0; i--) {
            double sum = 0;
            for (int j = i + 1; j < S.n; ++j)
                sum += X.mat[j][0] * S.mat[i][j];
            X.mat[i][0] = (Y.mat[i][0] - sum) / S.mat[i][i];
        }

        Matrix res = new Matrix(n, 1);
        for (int i = 0; i < n; i++) {
            res.mat[mis[i]][0] = X.mat[i][0];
        }
        return res;
    }

    public void swapCol(int a, int b) {
        if (RangeM(a) && RangeM(b)) {
            for (int i = 0; i < n; i++) {
                double t = mat[i][a];
                mat[i][a] = mat[i][b];
                mat[i][b] = t;
            }
            countSwap++;
        }
    }

    public void swapRow(int a, int b) {
        if (RangeN(a) && RangeN(b)) {
            for (int i = 0; i < m; i++) {
                double t = mat[a][i];
                mat[a][i] = mat[b][i];
                mat[b][i] = t;
            }
            countSwap++;
        }
    }

    public Matrix transformTrapezoid() {
        Matrix res = new Matrix(n, m);
        res.mat = getCopyMat();
        for (int i = 0; i < n; i++) {
            if (Math.abs(res.mat[i][i]) < eps) {
                int k = i + 1;
                while (k < n && Math.abs(res.mat[k][i]) < eps) k++;
                res.swapRow(i, k);
                res.countSwap++;
            }
            for (int j = i + 1; j < n; j++) {
                double coefficient = -res.mat[j][i] / res.mat[i][i];
                for (int k = 0; k < m; k++)
                    res.mat[j][k] = res.mat[j][k] + coefficient * res.mat[i][k];
            }
        }
        return res;
    }

    public Matrix transformTrapezoidWithMax() {
        Matrix res = new Matrix(n, m);
        res.mat = mat;
        for (int i = 0; i < n; i++) {
            int max;
            int k = i;
            while (k < n && Math.abs(res.mat[k][i]) < eps) k++;
            max = k;
            for (; k < n; k++)
                if (Math.abs(res.mat[k][i]) > Math.abs(res.mat[max][i]) && Math.abs(res.mat[i][i]) >= eps)
                    max = k;
            if (max != i)
                res.swapRow(i, max);

            for (int j = i + 1; j < n; j++) {

                double coefficient = -res.mat[j][i] / res.mat[i][i];
                for (int l = 0; l < m; l++)
                    res.mat[j][l] = res.mat[j][l] + coefficient * res.mat[i][l];
            }
        }
        return res;
    }

    public Matrix transposed() {
        Matrix res = new Matrix(m, n);
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                res.mat[j][i] = mat[i][j];
        return res;
    }
}