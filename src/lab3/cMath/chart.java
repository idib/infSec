package lab3.cMath;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.function.Function2D;
import org.jfree.data.function.NormalDistributionFunction2D;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

import java.util.List;

/**
 * This demo shows a normal distribution function.
 */
public class chart extends ApplicationFrame {
    int w = 600, h = 600;

    public chart(final String title, List<Func> funcs, double start, double finish) {
        super(title);
        init(createDataset(funcs, start, finish));
    }
    
    private void init(XYDataset DSet) {
        Function2D normal = new NormalDistributionFunction2D(0.0, 1.0);
        final JFreeChart chart = ChartFactory.createXYLineChart(
                "",
                "x",
                "y",
                DSet,
                PlotOrientation.VERTICAL,
                true,
                true,
                false
        );

        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(w, h));
        setContentPane(chartPanel);
    }

    public static void srun(String promt, List<Func> funcs, double start, double finish) {
        final chart demo = new chart(promt, funcs, start, finish);
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);
    }

    private XYDataset createDataset(List<Func> funcs, double start, double finish) {
        XYSeriesCollection result = new XYSeriesCollection();
        double n = ((finish - start) / w);
        
        for (int j = 0; j < funcs.size(); j++) {
            XYSeries series = new XYSeries(funcs.get(j).Name);
            for (javafx.geometry.Point2D p : funcs.get(j).getPoints(start, finish)) {
                series.add(p.getX(),p.getY());
            }
            result.addSeries(series);
        }
        return result;
    }
}